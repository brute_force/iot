export default {
  deviceTypes: [],
  devices: [],
  roles: [],
  grantedRoles: [],
  deviceData: [],
  newDeviceType: {
    name: "",
    description: "",
    encryptionEnabled: false,
    attributeTypes: []
  },
  ui: {
    showNewDeviceTypeModal: false,
    loading: 0,
    spinner: false,
    hasError: false,
    errorMessage: ""
  }
};
