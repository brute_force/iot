import { createStore, applyMiddleware } from "redux";
import reducers from "../reducers";
import reduxImmutableStateInvariant from "redux-immutable-state-invariant";
import createSagaMiddleware from "redux-saga";
import rootSaga from "../sagas";

const sagaMiddleware = createSagaMiddleware();

const middlewares = [sagaMiddleware];

const getMiddlewares = () =>
  process.env.NODE_ENV === "production"
    ? middlewares
    : middlewares.concat([reduxImmutableStateInvariant()]);

export default function configureStore(initialState) {
  const store = createStore(
    reducers,
    initialState,
    applyMiddleware(...getMiddlewares())
  );

  sagaMiddleware.run(rootSaga);

  return store;
}
