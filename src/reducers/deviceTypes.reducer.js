import * as actionTypes from "../actions/actionTypes";
import initialState from "../store/initialState";

export default function deviceTypesReducer(
  state = initialState.deviceTypes,
  action
) {
  switch (action.type) {
    case actionTypes.DEVICE_TYPES_READY:
      return action.payload;

    case actionTypes.DEVICE_TYPE_DETAILS_READY:
      return state.map(item => {
        if (item.id === action.payload.id) {
          return { ...item, ...action.payload, hasDetail: true };
        }
        return item;
      });

    case actionTypes.NEW_DEVICE_TYPE_READY:
      return [action.payload, ...state];

    case actionTypes.DELETE_DEVICE_TYPE_READY:
      return state.filter(i => i.id !== action.payload);

    default:
      return state;
  }
}
