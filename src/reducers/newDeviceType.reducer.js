import * as actionTypes from "../actions/actionTypes";
import initialState from "../store/initialState";

export default function newDeviceTypeReducer(
  state = initialState.newDeviceType,
  action
) {
  switch (action.type) {
    case actionTypes.ADD_NEW_DEVICE_TYPE:
      return action.payload;

    case actionTypes.NEW_DEVICE_TYPE_CHANGED:
      return action.payload;

    case actionTypes.NEW_DEVICE_TYPE_RESET:
      return initialState.newDeviceType;

    case actionTypes.NEW_ATTRIBUTE_TYPE_ADDED:
      const _state = { ...state };
      if (action.payload.type === "Enum") {
        action.payload.type = action.payload.array;
      }
      Reflect.deleteProperty(action.payload, "array");
      _state.attributeTypes = [...state.attributeTypes, action.payload];
      return _state;

    default:
      return state;
  }
}
