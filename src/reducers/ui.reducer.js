import * as actionTypes from "../actions/actionTypes";
import initialState from "../store/initialState";

export default function uiReducer(state = initialState.ui, action) {
  switch (action.type) {
    case actionTypes.TOGGLE_NEW_DEVICE_TYPE_MODAL:
      return {
        ...state,
        showNewDeviceTypeModal: !state.showNewDeviceTypeModal
      };

    case actionTypes.TOGGLE_LOADING:
      return {
        ...state,
        loading: !state.loading
      };

    case actionTypes.GET_DEVICE_TYPES:
    case actionTypes.GET_DEVICES:
    case actionTypes.GET_DEVICE_DATA:
    case actionTypes.GET_ROLES:
    case actionTypes.GET_GRANTED_ROLES:
    case actionTypes.ADD_NEW_DEVICE_TYPE:
    case actionTypes.ADD_NEW_DEVICE:
    case actionTypes.ADD_NEW_ROLE:
    case actionTypes.EDIT_DEVICE:
    case actionTypes.EDIT_ROLE:
    case actionTypes.DELETE_DEVICE_TYPE:
    case actionTypes.DELETE_DEVICE:
    case actionTypes.DELETE_ROLE:
      return {
        ...state,
        loading: state.loading + 1
      };

    case actionTypes.DEVICE_TYPES_READY:
    case actionTypes.DEVICES_READY:
    case actionTypes.DEVICE_DATA_READY:
    case actionTypes.ROLES_READY:
    case actionTypes.GRANTED_ROLES_READY:
    case actionTypes.NEW_DEVICE_TYPE_READY:
    case actionTypes.NEW_DEVICE_READY:
    case actionTypes.NEW_ROLE_READY:
    case actionTypes.EDIT_DEVICE_READY:
    case actionTypes.EDIT_ROLE_READY:
    case actionTypes.DELETE_DEVICE_TYPE_READY:
    case actionTypes.DELETE_DEVICE_READY:
    case actionTypes.DELETE_ROLE_READY:
      return {
        ...state,
        loading: state.loading - 1
      };

    case actionTypes.REQUEST_ERROR:
      return {
        ...state,
        loading: false,
        spinner: false,
        hasError: true,
        errorMessage: action.payload
      };

    case actionTypes.CLOSE_ERROR_MODAL:
      return {
        ...state,
        hasError: false,
        errorMessage: ""
      };

    case actionTypes.GRANT_ROLE:
    case actionTypes.TAKE_ROLE:
      return {
        ...state,
        spinner: true
      };

    case actionTypes.GRANT_ROLE_READY:
    case actionTypes.TAKE_ROLE_READY:
      return {
        ...state,
        spinner: false
      };

    default:
      return state;
  }
}
