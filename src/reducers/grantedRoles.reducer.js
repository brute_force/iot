import * as actionTypes from "../actions/actionTypes";
import initialState from "../store/initialState";

export default function grantedRolesReducer(
  state = initialState.grantedRoles,
  action
) {
  switch (action.type) {
    case actionTypes.GRANTED_ROLES_READY:
      return action.payload;

    case actionTypes.GRANT_ROLE_READY:
      return [action.payload, ...state];

    case actionTypes.TAKE_ROLE_READY:
      const { roleId, username, deviceId } = action.payload;
      return state.filter(
        gr =>
          gr.roleId !== roleId &&
          gr.username !== username &&
          gr.deviceId !== deviceId
      );

    case actionTypes.RESET_GRANTED_ROLES:
      return [];

    default:
      return state;
  }
}
