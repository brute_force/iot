import * as actionTypes from "../actions/actionTypes";
import initialState from "../store/initialState";

export default function rolesReducer(state = initialState.roles, action) {
  switch (action.type) {
    case actionTypes.ROLES_READY:
      return action.payload;

    case actionTypes.ROLE_DETAILS_READY:
      return state.map(item => {
        if (item.id === action.payload.id) {
          return { ...item, ...action.payload, hasDetail: true };
        }
        return item;
      });

    case actionTypes.EDIT_ROLE_READY:
      return state.map(role => {
        if (role.id === action.payload.id) {
          return {
            ...role,
            ...action.payload
          };
        }
        return role;
      });

    case actionTypes.NEW_ROLE_READY:
      return [action.payload, ...state];

    case actionTypes.DELETE_ROLE_READY:
      return state.filter(i => i.id !== action.payload);

    default:
      return state;
  }
}
