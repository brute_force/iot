import * as actionTypes from "../actions/actionTypes";
import initialState from "../store/initialState";

export default function deviceDataReducer(
  state = initialState.deviceData,
  action
) {
  switch (action.type) {
    case actionTypes.DEVICE_DATA_READY:
      return action.payload;

    // case actionTypes.WRITE_DEVICE_READY:
    //   return [action.payload, ...state];

    // case actionTypes.SEND_DEVICE_READY:
    //   return [action.payload, ...state];

    case actionTypes.RESET_DEVICE_DATA:
      return [];

    default:
      return state;
  }
}
