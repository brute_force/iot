import { combineReducers } from "redux";
import deviceTypesReducer from "./deviceTypes.reducer";
import devicesReducer from "./devices.reducer";
import rolesReducer from "./roles.reducer";
import grantedRolesReducer from "./grantedRoles.reducer";
import deviceDataReducer from "./deviceData.reducer";
import newDeviceTypeReducer from "./newDeviceType.reducer";
import uiReducer from "./ui.reducer";

export default combineReducers({
  deviceTypes: deviceTypesReducer,
  devices: devicesReducer,
  roles: rolesReducer,
  newDeviceType: newDeviceTypeReducer,
  grantedRoles: grantedRolesReducer,
  deviceData: deviceDataReducer,
  ui: uiReducer
});
