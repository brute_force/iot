import * as actionTypes from "../actions/actionTypes";
import initialState from "../store/initialState";

export default function devicesReducer(state = initialState.devices, action) {
  switch (action.type) {
    case actionTypes.DEVICES_READY:
      return action.payload;

    case actionTypes.DEVICE_DETAILS_READY:
      return state.map(item => {
        if (item.id === action.payload.id) {
          return { ...item, ...action.payload, hasDetail: true };
        }
        return item;
      });

    case actionTypes.EDIT_DEVICE_READY:
      return state.map(dev => {
        if (dev.id === action.payload.id) {
          return {
            ...dev,
            name: action.payload.name,
            serialNumber: action.payload.serialNumber
          };
        }
        return dev;
      });

    case actionTypes.NEW_DEVICE_READY:
      return [action.payload, ...state];

    case actionTypes.DELETE_DEVICE_READY:
      return state.filter(i => i.id !== action.payload);

    default:
      return state;
  }
}
