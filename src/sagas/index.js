import * as actionTypes from "../actions/actionTypes";
import { takeLatest, all } from "redux-saga/effects";
import * as deviceTypeSagas from "./deviceTypes.saga";
import * as deviceSagas from "./devices.saga";
import * as deviceDataSagas from "./deviceData.saga";
import * as roleSagas from "./roles.saga";

function* actionWatcher() {
  yield takeLatest(
    actionTypes.GET_DEVICE_TYPES,
    deviceTypeSagas.getDeviceTypes
  );
  yield takeLatest(
    actionTypes.GET_DEVICE_TYPE_DETAILS,
    deviceTypeSagas.getDeviceTypeDetails
  );
  yield takeLatest(
    actionTypes.ADD_NEW_DEVICE_TYPE,
    deviceTypeSagas.addNewDeviceType
  );
  yield takeLatest(
    actionTypes.DELETE_DEVICE_TYPE,
    deviceTypeSagas.deleteDeviceType
  );

  yield takeLatest(actionTypes.GET_DEVICES, deviceSagas.getDevices);
  yield takeLatest(
    actionTypes.GET_DEVICE_DETAILS,
    deviceSagas.getDeviceDetails
  );
  yield takeLatest(actionTypes.EDIT_DEVICE, deviceSagas.editDevice);
  yield takeLatest(actionTypes.ADD_NEW_DEVICE, deviceSagas.addNewDevice);
  yield takeLatest(actionTypes.DELETE_DEVICE, deviceSagas.deleteDevice);

  yield takeLatest(actionTypes.GET_ROLES, roleSagas.getRoles);
  yield takeLatest(actionTypes.GET_ROLE_DETAILS, roleSagas.getRoleDetails);
  yield takeLatest(actionTypes.EDIT_ROLE, roleSagas.editRole);
  yield takeLatest(actionTypes.ADD_NEW_ROLE, roleSagas.addNewRole);
  yield takeLatest(actionTypes.DELETE_ROLE, roleSagas.deleteRole);

  yield takeLatest(actionTypes.GET_GRANTED_ROLES, roleSagas.getGrantedRoles);
  yield takeLatest(actionTypes.GRANT_ROLE, roleSagas.grantRole);
  yield takeLatest(actionTypes.TAKE_ROLE, roleSagas.takeRole);

  yield takeLatest(actionTypes.GET_DEVICE_DATA, deviceDataSagas.getDeviceData);
  yield takeLatest(
    actionTypes.WRITE_DEVICE_DATA,
    deviceDataSagas.writeDeviceData
  );
  yield takeLatest(
    actionTypes.SEND_DEVICE_DATA,
    deviceDataSagas.sendDeviceData
  );
}

export default function* rootSaga() {
  yield all([actionWatcher()]);
}
