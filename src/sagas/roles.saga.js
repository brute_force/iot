import * as actionTypes from "../actions/actionTypes";
import { put } from "redux-saga/effects";
import axios from "axios";
import RoleApi from "../api/role.api";
import DeviceTypeApi from "../api/deviceType.api";

export function* getRoles(action) {
  try {
    const data = yield axios
      .all([RoleApi.get(action.payload), DeviceTypeApi.get()])
      .then(
        axios.spread((roleRes, dtRes) => {
          if (roleRes.data.message.statusCode === "SUCCESS") {
            return roleRes.data.data.roles.concat(dtRes.data.data.deviceTypes);
          }
          throw new Error(roleRes.data.message.statusMessage);
        })
      )
      .catch(error => {
        throw new Error(error.message);
      });
    yield put({ type: actionTypes.ROLES_READY, payload: data });
  } catch (error) {
    yield put({ type: actionTypes.REQUEST_ERROR, payload: error.message });
  }
}

export function* getRoleDetails(action) {
  try {
    const data = yield RoleApi.show(action.payload.id)
      .then(res => {
        if (res.data.message.statusCode === "SUCCESS") {
          return {
            id: action.payload.id,
            ...res.data.data,
            name: action.payload.name
          };
        }
        throw new Error(res.data.message.statusMessage);
      })
      .catch(error => {
        throw new Error(error.message);
      });
    yield put({ type: actionTypes.ROLE_DETAILS_READY, payload: data });
  } catch (error) {
    yield put({ type: actionTypes.REQUEST_ERROR, payload: error.message });
  }
}

export function* getGrantedRoles(action) {
  try {
    const data = yield RoleApi.getGrantedRoles(action.payload)
      .then(res => {
        if (res.data.message.statusCode === "SUCCESS") {
          return res.data.data.roles;
        }
        throw new Error(res.data.message.statusMessage);
      })
      .catch(error => {
        throw new Error(error.message);
      });
    yield put({ type: actionTypes.GRANTED_ROLES_READY, payload: data });
  } catch (error) {
    yield put({ type: actionTypes.REQUEST_ERROR, payload: error.message });
  }
}

export function* grantRole(action) {
  try {
    const data = yield RoleApi.grantRole(action.payload)
      .then(res => {
        if (res.data.message.statusCode === "SUCCESS") {
          return action.payload;
        }
        throw new Error(res.data.message.statusMessage);
      })
      .catch(error => {
        throw new Error(error.message);
      });
    yield put({ type: actionTypes.GRANT_ROLE_READY, payload: data });
  } catch (error) {
    yield put({ type: actionTypes.REQUEST_ERROR, payload: error.message });
  }
}

export function* takeRole(action) {
  try {
    const data = yield RoleApi.takeRole(action.payload)
      .then(res => {
        if (res.data.message.statusCode === "SUCCESS") {
          return action.payload;
        }
        throw new Error(res.data.message.statusMessage);
      })
      .catch(error => {
        throw new Error(error.message);
      });
    yield put({ type: actionTypes.TAKE_ROLE_READY, payload: data });
  } catch (error) {
    yield put({ type: actionTypes.REQUEST_ERROR, payload: error.message });
  }
}

export function* editRole(action) {
  try {
    const data = yield RoleApi.edit(action.payload)
      .then(res => {
        if (res.data.message.statusCode === "SUCCESS") {
          return { ...action.payload, ...res.data.data };
        }
        throw new Error(res.data.message.statusMessage);
      })
      .catch(error => {
        throw new Error(error.message);
      });
    yield put({ type: actionTypes.EDIT_ROLE_READY, payload: data });
  } catch (error) {
    yield put({ type: actionTypes.REQUEST_ERROR, payload: error.message });
  }
}

export function* addNewRole(action) {
  try {
    const data = yield RoleApi.add(action.payload)
      .then(res => {
        if (res.data.message.statusCode === "SUCCESS") {
          return { ...action.payload, ...res.data.data };
        }
        throw new Error(res.data.message.statusMessage);
      })
      .catch(error => {
        throw new Error(error.message);
      });
    yield put({ type: actionTypes.NEW_ROLE_READY, payload: data });
  } catch (error) {
    yield put({ type: actionTypes.REQUEST_ERROR, payload: error.message });
  }
}

export function* deleteRole(action) {
  try {
    const data = yield RoleApi.delete(action.payload)
      .then(res => {
        if (res.data.message.statusCode === "SUCCESS") {
          return res.data.data.id;
        }
        throw new Error(res.data.message.statusMessage);
      })
      .catch(error => {
        throw new Error(error.response.data.message.statusMessage);
      });
    yield put({ type: actionTypes.DELETE_ROLE_READY, payload: data });
  } catch (error) {
    yield put({ type: actionTypes.REQUEST_ERROR, payload: error.message });
  }
}
