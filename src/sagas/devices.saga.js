import * as actionTypes from "../actions/actionTypes";
import { put } from "redux-saga/effects";
import DeviceApi from "../api/device.api";

export function* getDevices(action) {
  try {
    const data = yield DeviceApi.get(action.payload)
      .then(res => {
        if (res.data.message.statusCode === "SUCCESS") {
          return res.data.data.devices;
        }
        throw new Error(res.data.message.statusMessage);
      })
      .catch(error => {
        throw new Error(error.message);
      });
    yield put({ type: actionTypes.DEVICES_READY, payload: data });
  } catch (error) {
    yield put({ type: actionTypes.REQUEST_ERROR, payload: error.message });
  }
}

export function* getDeviceDetails(action) {
  try {
    const data = yield DeviceApi.show(action.payload)
      .then(res => {
        if (res.data.message.statusCode === "SUCCESS") {
          return res.data.data;
        }
        throw new Error(res.data.message.statusMessage);
      })
      .catch(error => {
        throw new Error(error.message);
      });
    yield put({ type: actionTypes.DEVICE_DETAILS_READY, payload: data });
  } catch (error) {
    yield put({ type: actionTypes.REQUEST_ERROR, payload: error.message });
  }
}

export function* editDevice(action) {
  try {
    const data = yield DeviceApi.edit(action.payload)
      .then(res => {
        if (res.data.message.statusCode === "SUCCESS") {
          return { ...action.payload, ...res.data.data };
        }
        throw new Error(res.data.message.statusMessage);
      })
      .catch(error => {
        throw new Error(error.message);
      });
    yield put({ type: actionTypes.EDIT_DEVICE_READY, payload: data });
  } catch (error) {
    yield put({ type: actionTypes.REQUEST_ERROR, payload: error.message });
  }
}

export function* addNewDevice(action) {
  try {
    const data = yield DeviceApi.add(action.payload)
      .then(res => {
        if (res.data.message.statusCode === "SUCCESS") {
          return { ...action.payload, ...res.data.data };
        }
        throw new Error(res.data.message.statusMessage);
      })
      .catch(error => {
        throw new Error(error.message);
      });
    yield put({ type: actionTypes.NEW_DEVICE_READY, payload: data });
  } catch (error) {
    yield put({ type: actionTypes.REQUEST_ERROR, payload: error.message });
  }
}

export function* deleteDevice(action) {
  try {
    const data = yield DeviceApi.delete(action.payload)
      .then(res => {
        if (res.data.message.statusCode === "SUCCESS") {
          return res.data.data.id;
        }
        throw new Error(res.data.message.statusMessage);
      })
      .catch(error => {
        throw new Error(error.message);
      });
    yield put({ type: actionTypes.DELETE_DEVICE_READY, payload: data });
  } catch (error) {
    yield put({ type: actionTypes.REQUEST_ERROR, payload: error.message });
  }
}
