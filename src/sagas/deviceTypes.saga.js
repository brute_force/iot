import * as actionTypes from "../actions/actionTypes";
import { put } from "redux-saga/effects";
import DeviceTypeApi from "../api/deviceType.api";

export function* getDeviceTypes(action) {
  try {
    const data = yield DeviceTypeApi.get(action.payload)
      .then(res => {
        if (res.data.message.statusCode === "SUCCESS") {
          return res.data.data.deviceTypes;
        }
        throw new Error(res.data.message.statusMessage);
      })
      .catch(error => {
        throw new Error(error.message);
      });
    yield put({ type: actionTypes.DEVICE_TYPES_READY, payload: data });
  } catch (error) {
    yield put({ type: actionTypes.REQUEST_ERROR, payload: error.message });
  }
}

export function* getDeviceTypeDetails(action) {
  try {
    const data = yield DeviceTypeApi.show(action.payload)
      .then(res => {
        if (res.data.message.statusCode === "SUCCESS") {
          return res.data.data;
        }
        throw new Error(res.data.message.statusMessage);
      })
      .catch(error => {
        throw new Error(error.message);
      });
    yield put({ type: actionTypes.DEVICE_TYPE_DETAILS_READY, payload: data });
  } catch (error) {
    yield put({ type: actionTypes.REQUEST_ERROR, payload: error.message });
  }
}

export function* addNewDeviceType(action) {
  try {
    const data = yield DeviceTypeApi.add(action.payload)
      .then(res => {
        if (res.data.message.statusCode === "SUCCESS") {
          return { ...action.payload, id: res.data.data.id };
        }
        throw new Error(res.data.message.statusMessage);
      })
      .catch(error => {
        throw new Error(error.message);
      });
    yield put({ type: actionTypes.NEW_DEVICE_TYPE_READY, payload: data });
  } catch (error) {
    yield put({ type: actionTypes.REQUEST_ERROR, payload: error.message });
  }
}

export function* deleteDeviceType(action) {
  try {
    const data = yield DeviceTypeApi.delete(action.payload)
      .then(res => {
        if (res.data.message.statusCode === "SUCCESS") {
          return res.data.data.id;
        }
        throw new Error(res.data.message.statusMessage);
      })
      .catch(error => {
        throw new Error(error.message);
      });
    yield put({ type: actionTypes.DELETE_DEVICE_TYPE_READY, payload: data });
  } catch (error) {
    yield put({ type: actionTypes.REQUEST_ERROR, payload: error.message });
  }
}
