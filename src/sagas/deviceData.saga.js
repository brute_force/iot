import * as actionTypes from "../actions/actionTypes";
import { put } from "redux-saga/effects";
import DeviceDataApi from "../api/deviceData.api";

export function* getDeviceData(action) {
  try {
    const data = yield DeviceDataApi.get(action.payload)
      .then(res => {
        if (res.data.message.statusCode === "SUCCESS") {
          return res.data.data.attributes;
        }
        throw new Error(res.data.message.statusMessage);
      })
      .catch(error => {
        throw new Error(error.message);
      });
    yield put({ type: actionTypes.DEVICE_DATA_READY, payload: data });
  } catch (error) {
    yield put({ type: actionTypes.REQUEST_ERROR, payload: error.message });
  }
}

export function* writeDeviceData(action) {
  try {
    const data = yield DeviceDataApi.write(action.payload)
      .then(res => {
        if (res.data.message.statusCode === "SUCCESS") {
          return action.payload;
        }
        throw new Error(res.data.message.statusMessage);
      })
      .catch(error => {
        throw new Error(error.message);
      });
    yield put({ type: actionTypes.WRITE_DEVICE_READY, payload: data });
  } catch (error) {
    yield put({ type: actionTypes.REQUEST_ERROR, payload: error.message });
  }
}

export function* sendDeviceData(action) {
  try {
    const data = yield DeviceDataApi.send(action.payload)
      .then(res => {
        if (res.data.message.statusCode === "SUCCESS") {
          return action.payload;
        }
        throw new Error(res.data.message.statusMessage);
      })
      .catch(error => {
        throw new Error(error.message);
      });
    yield put({ type: actionTypes.SEND_DEVICE_READY, payload: data });
  } catch (error) {
    yield put({ type: actionTypes.REQUEST_ERROR, payload: error.message });
  }
}
