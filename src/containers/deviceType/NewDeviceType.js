import React, { Component } from "react";
import NewDeviceTypeForm from "../../components/deviceType/NewDeviceTypeForm";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as actions from "../../actions/deviceTypeActions";
import * as uiActions from "../../actions/uiActions";

class NewDeviceType extends Component {
  updateDevice = e => {
    const device = { ...this.props.newDeviceType };
    device[e.target.name] = e.target.value;
    if (e.target.name === "encryptionEnabled") {
      device[e.target.name] = e.target.value === "true";
    }
    this.props.actions.newDeviceTypeChanged(device);
  };

  saveNewDeviceType = () => {
    this.props.actions.addNewDeviceType(this.props.newDeviceType);
    this.onModalClosed();
  };

  onModalClosed = () => {
    this.props.actions.newDeviceTypeReset();
    this.props.actions.toggleNewDeviceTypeModal();
  };

  render() {
    const { showNewDeviceTypeModal } = this.props;
    return (
      <div
        id="add-new-device"
        className={showNewDeviceTypeModal ? "overlay active" : "overlay"}
      >
        <div className="add-new-device">
          <div className="back-to-list" onClick={this.onModalClosed}>
            <i className="iot-go-back-left-arrow" />
            <span>back to list</span>
          </div>
          <h2 className="add-new-device-title">New Device Type</h2>
          <NewDeviceTypeForm
            device={this.props.newDeviceType}
            onChange={this.updateDevice}
            onSave={this.saveNewDeviceType}
            onCancel={this.onModalClosed}
          />
        </div>
      </div>
    );
  }
}

function mapStateToProps(state, props) {
  return {
    newDeviceType: state.newDeviceType,
    showNewDeviceTypeModal: state.ui.showNewDeviceTypeModal
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ ...actions, ...uiActions }, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NewDeviceType);
