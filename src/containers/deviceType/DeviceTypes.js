import React, { Component } from "react";
import Navbar from "../../components/Navbar";
import Titlebar from "../../components/Titlebar";
import Confirm from "../../components/UI/Confirm";
import DeviceTypeList from "../../components/deviceType/DeviceTypeList";
import NewDeviceType from "./NewDeviceType";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as uiActions from "../../actions/uiActions";
import * as actions from "../../actions/deviceTypeActions";

class DeviceTypes extends Component {
  state = { isConfirmVisible: false, idToDelete: "" };

  componentDidMount() {
    this.props.actions.getDeviceTypes();
  }

  onShowDetails = item => {
    if (!item.hasDetail) {
      this.props.actions.getDeviceTypeDetails(item.id);
    }
  };

  toggleConfirm = visibility => {
    this.setState({ isConfirmVisible: visibility || false });
  };

  onConfirm = isVisible => {
    this.toggleConfirm();
    this.props.actions.deleteDeviceType(this.state.idToDelete);
    this.setState({ idToDelete: "" });
  };

  onDeleteItem = id => {
    this.toggleConfirm(true);
    this.setState({ idToDelete: id });
  };

  render() {
    return (
      <div>
        <Confirm
          isVisible={this.state.isConfirmVisible}
          message="Are you sure you want to delete this item?"
          onConfirm={this.onConfirm}
          onCancel={() => this.setState({ isConfirmVisible: false })}
        />
        <NewDeviceType />
        <Navbar activeRoute={this.props.match.path} />
        <Titlebar
          count={this.props.devicetypes.length}
          name={"Device type"}
          toggleNewModal={this.props.actions.toggleNewDeviceTypeModal}
        />
        <section className="items-table">
          <div className="items-table-header">
            <i>Row</i>
            <i>Name</i>
            <i>Actions</i>
          </div>
          <div className="items-accordion">
            <DeviceTypeList
              list={this.props.devicetypes}
              showDetails={this.onShowDetails}
              onDeleteItem={this.onDeleteItem}
            />
          </div>
        </section>
      </div>
    );
  }
}

function mapStateToProps(state, props) {
  return {
    devicetypes: state.deviceTypes,
    showNewDeviceTypeModal: state.ui.showNewDeviceTypeModal
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ ...uiActions, ...actions }, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DeviceTypes);
