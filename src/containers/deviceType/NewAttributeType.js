import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as actions from "../../actions/deviceTypeActions";

class NewAttributeType extends Component {
  state = {
    name: "",
    type: "String",
    array: []
  };

  updateState = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  isSelected = type => this.state.type === type;

  addAttribute = () => {
    const state = this.state;
    if (this.state.type === "Enum") {
      try {
        state.array = JSON.parse(this.state.array);
      } catch (error) {
        alert("آرایه ورودی اشتباه است");
        return;
      }
    }
    this.props.actions.newAttributeTypeAdded({ ...state });
    this.setState({
      name: "",
      type: "String",
      array: []
    });
  };

  render() {
    return (
      <div className="add-attr">
        <input
          type="text"
          name="name"
          placeholder="Name"
          value={this.state.name}
          onChange={this.updateState}
        />
        <select
          name="type"
          placeholder="Type"
          value={this.state.type}
          onChange={this.updateState}
        >
          <option value="String">String</option>
          <option value="Number">Number</option>
          <option value="Boolean">Boolean</option>
          <option value="Enum">Enum</option>
        </select>
        <input
          type="text"
          name="array"
          placeholder="Array"
          value={this.state.array}
          onChange={this.updateState}
          style={{
            display: this.state.type !== "Enum" ? "none" : "inline-block"
          }}
        />
        <button
          type="button"
          className="border-btn add-attr-btn"
          onClick={this.addAttribute}
        >
          + Add
        </button>
      </div>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  };
}

export default connect(
  null,
  mapDispatchToProps
)(NewAttributeType);
