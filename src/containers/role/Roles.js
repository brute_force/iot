import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import Navbar from "../../components/Navbar";
import Confirm from "../../components/UI/Confirm";
import Titlebar from "../../components/Titlebar";
import RoleList from "../../components/role/RoleList";
import NewRoleForm from "../../components/role/NewRoleForm";
import EditRoleForm from "../../components/role/EditRoleForm";
import Modal from "../../components/UI/Modal";
import * as uiActions from "../../actions/uiActions";
import * as actions from "../../actions/roleActions";
import { getDeviceTypes } from "../../actions/deviceTypeActions";

class Roles extends Component {
  selectedID = "";
  editing = false;

  state = {
    roleToEdit: {},
    isConfirmVisible: false,
    isNewModalVisible: false,
    isEditModalVisible: false
  };

  componentDidMount() {
    this.props.actions.getRoles();
    this.props.actions.getDeviceTypes();
  }

  componentDidUpdate() {
    if (this.editing && this.selectedID) {
      this.editing = false;
      this.props.actions.toggleLoading();
      const item = this.props.roles.find(r => r.id === this.selectedID);
      this.selectedID = "";
      this.onEditItem(item);
    }
  }

  addRole = role => {
    this.props.actions.addRole(role);
    this.toggleNewModal();
  };

  onShowDetails = item => {
    if (!item.hasDetail) {
      this.props.actions.getRoleDetails(item);
    }
  };

  toggleConfirm = visibility => {
    this.setState({ isConfirmVisible: visibility || false });
  };

  onConfirm = () => {
    this.toggleConfirm();
    this.props.actions.deleteRole(this.selectedID);
    this.selectedID = "";
  };

  toggleNewModal = () => {
    this.setState(({ isNewModalVisible }) => ({
      isNewModalVisible: !isNewModalVisible
    }));
  };

  closeEditModal = () => {
    this.setState(({ isEditModalVisible }) => ({
      roleToEdit: {},
      isEditModalVisible: !isEditModalVisible
    }));
  };

  onDeleteItem = id => {
    this.toggleConfirm(true);
    this.selectedID = id;
  };

  onEditItem = item => {
    if (item.hasDetail) {
      this.setState({
        roleToEdit: item,
        isEditModalVisible: true
      });
    } else {
      this.editing = true;
      this.selectedID = item.id;
      this.props.actions.toggleLoading();
      this.onShowDetails(item);
    }
  };

  editRole = role => {
    role.deviceTypeName = this.props.deviceTypes.find(
      d => role.deviceTypeId === d.id
    ).name;
    this.props.actions.editRole(role);
    this.setState({
      roleToEdit: {},
      isEditModalVisible: false
    });
    this.editing = false;
    this.selectedID = "";
  };

  render() {
    return (
      <div>
        <Confirm
          isVisible={this.state.isConfirmVisible}
          message="Are you sure you want to delete this item?"
          onConfirm={this.onConfirm}
          onCancel={() => this.setState({ isConfirmVisible: false })}
        />
        <Modal
          isVisible={this.state.isNewModalVisible}
          toggleVisibility={this.toggleNewModal}
          title="New Role"
        >
          <NewRoleForm
            deviceTypes={this.props.deviceTypes}
            onSave={this.addRole}
            onCancel={this.toggleNewModal}
          />
        </Modal>
        <Modal
          isVisible={this.state.isEditModalVisible}
          toggleVisibility={this.closeEditModal}
          title="Edit Role"
        >
          <EditRoleForm
            role={this.state.roleToEdit}
            deviceTypes={this.props.deviceTypes}
            onSave={this.editRole}
            onCancel={this.closeEditModal}
          />
        </Modal>
        <Navbar activeRoute={this.props.match.path} />
        <Titlebar
          count={this.props.roles.length}
          name={"Role"}
          toggleNewModal={this.toggleNewModal}
        />
        <section className="items-table">
          <div className="items-table-header">
            <i>Row</i>
            <i>Name</i>
            <i>Actions</i>
          </div>
          <div className="items-accordion">
            <RoleList
              list={this.props.roles}
              showDetails={this.onShowDetails}
              onDeleteItem={this.onDeleteItem}
              onEditItem={this.onEditItem}
            />
          </div>
        </section>
      </div>
    );
  }
}

function mapStateToProps(state, props) {
  return {
    ...props,
    roles: state.roles,
    deviceTypes: state.deviceTypes
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(
      { ...uiActions, ...actions, getDeviceTypes },
      dispatch
    )
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Roles);
