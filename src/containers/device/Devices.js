import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import Navbar from "../../components/Navbar";
import Confirm from "../../components/UI/Confirm";
import Titlebar from "../../components/Titlebar";
import DeviceList from "../../components/device/DeviceList";
import NewDeviceForm from "../../components/device/NewDeviceForm";
import EditDeviceForm from "../../components/device/EditDeviceForm";
import UserSandbox from "../../components/device/UserSandbox";
import DeviceData from "../../components/device/DeviceData";
import Modal from "../../components/UI/Modal";
import * as uiActions from "../../actions/uiActions";
import * as actions from "../../actions/deviceActions";
import { getDeviceTypes } from "../../actions/deviceTypeActions";
import { resetGrantedRoles } from "../../actions/grantedRolesActions";
import { resetDeviceData } from "../../actions/deviceDataActions";
import { getRoles } from "../../actions/roleActions";

class Devices extends Component {
  selectedID = "";
  selectedDevice = "";
  editing = false;

  state = {
    deviceToEdit: {
      id: "",
      name: "",
      serialNumber: ""
    },
    isConfirmVisible: false,
    isNewDeviceModalVisible: false,
    isEditModalVisible: false,
    isSandboxVisible: false,
    isDataModalVisible: false
  };

  componentWillUnmount() {
    this.selectedID = this.selectedDevice = "";
    this.editing = false;
  }

  componentDidMount() {
    this.props.actions.getDevices();
    this.props.actions.getDeviceTypes();
    this.props.actions.getRoles();
  }

  componentDidUpdate() {
    if (this.editing && this.selectedID) {
      const deviceToEdit = this.props.devices.find(
        i => i.id === this.selectedID
      );
      this.selectedID = "";
      this.props.actions.toggleLoading();
      this.onEditItem(deviceToEdit);
    }
  }

  addNewDevice = device => {
    this.props.actions.addNewDevice(device);
    this.toggleNewDeviceModal();
  };

  editDevice = () => {
    this.props.actions.editDevice(this.state.deviceToEdit);
    this.setState({
      deviceToEdit: {
        id: "",
        name: "",
        serialNumber: ""
      },
      isEditModalVisible: false
    });
    this.selectedID = "";
    this.editing = false;
  };

  onShowDetails = item => {
    if (!item.hasDetail) {
      this.props.actions.getDeviceDetails(item.id);
    }
  };

  toggleConfirm = visibility => {
    this.setState({ isConfirmVisible: visibility || false });
  };

  onConfirm = () => {
    this.toggleConfirm();
    this.props.actions.deleteDevice(this.selectedID);
    this.selectedID = "";
  };

  toggleNewDeviceModal = () => {
    this.setState(({ isNewDeviceModalVisible }) => ({
      isNewDeviceModalVisible: !isNewDeviceModalVisible
    }));
  };

  toggleEditModal = () => {
    this.setState(({ isEditModalVisible }) => ({
      isEditModalVisible: !isEditModalVisible
    }));
    if (!this.state.isEditModalVisible) {
      this.selectedID = "";
      this.editing = false;
    }
  };

  closeUserSandboxModal = () => {
    this.selectedID = "";
    this.setState({
      isSandboxVisible: false
    });
    this.props.actions.resetGrantedRoles();
  };

  onDeleteItem = id => {
    this.toggleConfirm(true);
    this.selectedID = id;
  };

  updateEditingDevice = ({ target }) => {
    this.setState(({ deviceToEdit }) => ({
      deviceToEdit: {
        ...deviceToEdit,
        [target.name]: target.value
      }
    }));
  };

  onEditItem = item => {
    if (!item.hasDetail) {
      this.selectedID = item.id;
      this.props.actions.getDeviceDetails(item.id);
      this.props.actions.toggleLoading();
    } else {
      this.setState({
        deviceToEdit: {
          id: item.id,
          name: item.name,
          serialNumber: item.serialNumber
        },
        isEditModalVisible: true
      });
    }
    this.editing = true;
  };

  viewSandbox = item => {
    this.selectedID = item.id;
    this.setState({
      isSandboxVisible: true
    });
  };

  closeDataModal = () => {
    this.selectedDevice = "";
    this.setState({
      isDataModalVisible: false
    });
    this.props.actions.resetDeviceData();
  };

  viewDataModal = item => {
    this.selectedDevice = item;
    this.setState({
      isDataModalVisible: true
    });
  };

  render() {
    return (
      <div>
        <Confirm
          isVisible={this.state.isConfirmVisible}
          message="Are you sure you want to delete this item?"
          onConfirm={this.onConfirm}
          onCancel={() => this.setState({ isConfirmVisible: false })}
        />
        <Modal
          isVisible={this.state.isNewDeviceModalVisible}
          toggleVisibility={this.toggleNewDeviceModal}
          title="New Device"
        >
          <NewDeviceForm
            isVisible={this.state.isNewDeviceModalVisible}
            deviceTypes={this.props.deviceTypes}
            onSave={this.addNewDevice}
            onCancel={this.toggleNewDeviceModal}
          />
        </Modal>
        <Modal
          isVisible={this.state.isEditModalVisible}
          toggleVisibility={this.toggleEditModal}
          title="Edit Device"
        >
          <EditDeviceForm
            isVisible={this.state.isEditModalVisible}
            device={this.state.deviceToEdit}
            updateDevice={this.updateEditingDevice}
            onSave={this.editDevice}
            onCancel={this.toggleEditModal}
          />
        </Modal>
        <Modal
          isVisible={this.state.isSandboxVisible}
          toggleVisibility={this.closeUserSandboxModal}
          title="User Sandbox"
        >
          <UserSandbox
            isVisible={this.state.isSandboxVisible}
            deviceId={this.selectedID}
            roles={this.props.roles}
            onCancel={this.closeUserSandboxModal}
          />
        </Modal>
        <Modal
          isVisible={this.state.isDataModalVisible}
          toggleVisibility={this.closeDataModal}
          title="Data Sandbox"
        >
          <DeviceData
            isVisible={this.state.isDataModalVisible}
            device={this.selectedDevice}
            onCancel={this.closeDataModal}
          />
        </Modal>
        <Navbar activeRoute={this.props.match.path} />
        <Titlebar
          count={this.props.devices.length}
          name={"Device"}
          toggleNewModal={this.toggleNewDeviceModal}
        />
        <section className="items-table">
          <div className="items-table-header">
            <i>Row</i>
            <i>Name</i>
            <i>Actions</i>
          </div>
          <div className="items-accordion">
            <DeviceList
              list={this.props.devices.filter(i => i.isOwned)}
              showDetails={this.onShowDetails}
              onDeleteItem={this.onDeleteItem}
              onEditItem={this.onEditItem}
              onSandbox={this.viewSandbox}
              onDataModal={this.viewDataModal}
            />
          </div>
        </section>
        <section className="items-table" style={{ marginTop: "20px" }}>
          <div className="items-table-header">
            <i>Granted Devices</i>
          </div>
          <div className="items-accordion">
            <DeviceList
              list={this.props.devices.filter(i => !i.isOwned)}
              showDetails={this.onShowDetails}
              onDeleteItem={this.onDeleteItem}
            />
          </div>
        </section>
      </div>
    );
  }
}

function mapStateToProps(state, props) {
  return {
    ...props,
    roles: state.roles,
    devices: state.devices,
    deviceTypes: state.deviceTypes
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(
      {
        ...uiActions,
        ...actions,
        getDeviceTypes,
        resetGrantedRoles,
        resetDeviceData,
        getRoles
      },
      dispatch
    )
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Devices);
