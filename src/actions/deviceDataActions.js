import * as actionTypes from "./actionTypes";

export function getDeviceData(payload) {
  return {
    type: actionTypes.GET_DEVICE_DATA,
    payload
  };
}

export function sendToDevice(payload) {
  return {
    type: actionTypes.SEND_DEVICE_DATA,
    payload
  };
}

export function writeToDevice(payload) {
  return {
    type: actionTypes.WRITE_DEVICE_DATA,
    payload
  };
}

export function resetDeviceData() {
  return {
    type: actionTypes.RESET_DEVICE_DATA
  };
}
