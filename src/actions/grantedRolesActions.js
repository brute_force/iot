import * as actionTypes from "./actionTypes";

export function getGrantedRoles(payload) {
  return {
    type: actionTypes.GET_GRANTED_ROLES,
    payload
  };
}

export function grantRole(payload) {
  return {
    type: actionTypes.GRANT_ROLE,
    payload
  };
}

export function takeRole(payload) {
  return {
    type: actionTypes.TAKE_ROLE,
    payload
  };
}

export function resetGrantedRoles() {
  return {
    type: actionTypes.RESET_GRANTED_ROLES
  };
}
