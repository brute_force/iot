import * as actionTypes from "./actionTypes";

export function getRoles(payload) {
  return {
    type: actionTypes.GET_ROLES,
    payload
  };
}

export function getRoleDetails(payload) {
  return {
    type: actionTypes.GET_ROLE_DETAILS,
    payload
  };
}

export function editRole(payload) {
  return {
    type: actionTypes.EDIT_ROLE,
    payload
  };
}

export function addRole(payload) {
  return {
    type: actionTypes.ADD_NEW_ROLE,
    payload
  };
}

export function deleteRole(payload) {
  return {
    type: actionTypes.DELETE_ROLE,
    payload
  };
}
