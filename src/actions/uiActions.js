import * as actionTypes from "./actionTypes";

export function toggleNewDeviceTypeModal() {
  return {
    type: actionTypes.TOGGLE_NEW_DEVICE_TYPE_MODAL
  };
}

export function toggleLoading() {
  return {
    type: actionTypes.TOGGLE_LOADING
  };
}

export function closeErrorModal() {
  return {
    type: actionTypes.CLOSE_ERROR_MODAL
  };
}
