import * as actionTypes from "./actionTypes";

export function getDevices(payload) {
  return {
    type: actionTypes.GET_DEVICES,
    payload
  };
}

export function getDeviceDetails(payload) {
  return {
    type: actionTypes.GET_DEVICE_DETAILS,
    payload
  };
}

export function editDevice(payload) {
  return {
    type: actionTypes.EDIT_DEVICE,
    payload
  };
}

export function addNewDevice(payload) {
  return {
    type: actionTypes.ADD_NEW_DEVICE,
    payload
  };
}

export function deleteDevice(payload) {
  return {
    type: actionTypes.DELETE_DEVICE,
    payload
  };
}
