import * as actionTypes from "./actionTypes";

export function getDeviceTypes(payload) {
  return {
    type: actionTypes.GET_DEVICE_TYPES,
    payload
  };
}

export function getDeviceTypeDetails(payload) {
  return {
    type: actionTypes.GET_DEVICE_TYPE_DETAILS,
    payload
  };
}

export function addNewDeviceType(payload) {
  return {
    type: actionTypes.ADD_NEW_DEVICE_TYPE,
    payload
  };
}

export function deleteDeviceType(payload) {
  return {
    type: actionTypes.DELETE_DEVICE_TYPE,
    payload
  };
}

export function newDeviceTypeChanged(payload) {
  return {
    type: actionTypes.NEW_DEVICE_TYPE_CHANGED,
    payload
  };
}

export function newDeviceTypeReset() {
  return {
    type: actionTypes.NEW_DEVICE_TYPE_RESET
  };
}

export function newAttributeTypeAdded(payload) {
  return {
    type: actionTypes.NEW_ATTRIBUTE_TYPE_ADDED,
    payload
  };
}
