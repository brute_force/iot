import axios from "./axios";
import { refreshToken, objectToFormData, objectToQS } from "../util";

const root = "device";

export default class DeviceAPI {
  static get(params = {}) {
    return refreshToken().then(() =>
      axios.get(root, {
        params,
        headers: {
          userToken: localStorage.getItem("userToken"),
          timeStamp: Date.now()
        }
      })
    );
  }

  static show(payload) {
    return refreshToken().then(() =>
      axios.get(`${root}/${payload}`, {
        headers: {
          userToken: localStorage.getItem("userToken"),
          timeStamp: Date.now()
        }
      })
    );
  }

  static edit(payload) {
    return refreshToken().then(() => {
      const data = objectToQS({
        name: payload.name,
        serialNumber: payload.serialNumber
      });
      return axios.put(`${root}/${payload.id}?${data}`, null, {
        headers: {
          userToken: localStorage.getItem("userToken"),
          timeStamp: Date.now()
        }
      });
    });
  }

  static add(payload) {
    return refreshToken().then(() => {
      const data = objectToFormData(payload);
      return axios.post(root, data, {
        headers: {
          userToken: localStorage.getItem("userToken"),
          timeStamp: Date.now()
        }
      });
    });
  }

  static delete(payload) {
    return refreshToken().then(() =>
      axios.delete(`${root}/${payload}`, {
        headers: {
          userToken: localStorage.getItem("userToken"),
          timeStamp: Date.now()
        }
      })
    );
  }
}
