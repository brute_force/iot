import axios from "axios";

const instance = axios.create({
  baseURL: process.env.NODE_ENV === "production" ? "http://api.thingscloud.ir/srv/iot/" : "/"
});

export default instance;
