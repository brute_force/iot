import axios from "./axios";
import { refreshToken, objectToQS } from "../util";

const root = "deviceData";

export default class DeviceDataAPI {
  static get(deviceId) {
    return refreshToken().then(() =>
      axios.get(`${root}/${deviceId}`, {
        headers: {
          userToken: localStorage.getItem("userToken"),
          timeStamp: Date.now()
        }
      })
    );
  }

  static write(payload) {
    return refreshToken().then(() => {
      const data = objectToQS({
        attributes: payload.write
      });
      return axios.post(`${root}/${payload.deviceId}?${data}`, null, {
        headers: {
          userToken: localStorage.getItem("userToken"),
          timeStamp: Date.now()
        }
      });
    });
  }

  static send(payload) {
    return refreshToken().then(() => {
      const data = objectToQS({
        attributes: payload.send
      });
      return axios.post(`${root}/todevice/${payload.deviceId}?${data}`, null, {
        headers: {
          userToken: localStorage.getItem("userToken"),
          timeStamp: Date.now()
        }
      });
    });
  }
}
