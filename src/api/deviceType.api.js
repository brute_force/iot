import axios from "./axios";
import { refreshToken, objectToFormData } from "../util";

const root = "devicetype";

export default class DeviceTypeAPI {
  static get(params = {}) {
    return refreshToken().then(() =>
      axios.get(root, {
        params,
        headers: {
          userToken: localStorage.getItem("userToken"),
          timeStamp: Date.now()
        }
      })
    );
  }

  static show(payload) {
    return refreshToken().then(() =>
      axios.get(`${root}/${payload}`, {
        headers: {
          userToken: localStorage.getItem("userToken"),
          timeStamp: Date.now()
        }
      })
    );
  }

  static add(payload) {
    return refreshToken().then(() => {
      payload.encryptionEnabled =
        payload.encryptionEnabled === "true" ? true : false;
      const data = objectToFormData(payload);
      return axios.post(root, data, {
        headers: {
          userToken: localStorage.getItem("userToken"),
          timeStamp: Date.now()
        }
      });
    });
  }

  static delete(payload) {
    return refreshToken().then(() =>
      axios.delete(`${root}/${payload}`, {
        headers: {
          userToken: localStorage.getItem("userToken"),
          timeStamp: Date.now()
        }
      })
    );
  }
}
