import axios from "./axios";
import { refreshToken, objectToFormData, objectToQS } from "../util";

const root = "role";

export default class RoleAPI {
  static get(params = {}) {
    return refreshToken().then(() =>
      axios.get(root, {
        params,
        headers: {
          userToken: localStorage.getItem("userToken"),
          timeStamp: Date.now()
        }
      })
    );
  }

  static getGrantedRoles(deviceId, params = {}) {
    return refreshToken().then(() =>
      axios.get(`${root}/grant?deviceId=${deviceId}`, {
        params,
        headers: {
          userToken: localStorage.getItem("userToken"),
          timeStamp: Date.now()
        }
      })
    );
  }

  static show(payload) {
    return refreshToken().then(() =>
      axios.get(`${root}/${payload}`, {
        headers: {
          userToken: localStorage.getItem("userToken"),
          timeStamp: Date.now()
        }
      })
    );
  }

  static edit({ id, name, description, attributePermissions }) {
    return refreshToken().then(() => {
      const data = objectToQS({ name, description, attributePermissions });
      return axios.put(`${root}/${id}?${data}`, null, {
        headers: {
          userToken: localStorage.getItem("userToken"),
          timeStamp: Date.now()
        }
      });
    });
  }

  static add(payload) {
    return refreshToken().then(() => {
      const data = objectToFormData(payload);
      return axios.post(root, data, {
        headers: {
          userToken: localStorage.getItem("userToken"),
          timeStamp: Date.now()
        }
      });
    });
  }

  static grantRole(role) {
    return refreshToken().then(() => {
      const data = objectToQS(role);
      return axios.post(`${root}/grant?${data}`, null, {
        headers: {
          userToken: localStorage.getItem("userToken"),
          timeStamp: Date.now()
        }
      });
    });
  }

  static takeRole(role) {
    return refreshToken().then(() => {
      const data = objectToQS(role);
      return axios.post(`${root}/take?${data}`, null, {
        headers: {
          userToken: localStorage.getItem("userToken"),
          timeStamp: Date.now()
        }
      });
    });
  }

  static delete(payload) {
    return refreshToken().then(() =>
      axios.delete(`${root}/${payload}`, {
        headers: {
          userToken: localStorage.getItem("userToken"),
          timeStamp: Date.now()
        }
      })
    );
  }
}
