import React, { Component } from "react";
import TextInput from "../UI/TextInput";

class NewDeviceForm extends Component {
  state = {
    device: {
      name: "",
      label: "",
      deviceTypeId: "",
      serialNumber: "",
      pushUrl: "",
      isOwned: true
    }
  };

  shouldComponentUpdate(nextProps) {
    return nextProps.isVisible;
  }

  updateDevice = ({ target }) => {
    const device = { ...this.state.device, [target.name]: target.value };
    this.setState({
      device
    });
  };

  addNewDevice = () => {
    this.props.onSave(this.state.device);
    this.setState({
      device: {
        name: "",
        label: "",
        deviceTypeId: "",
        serialNumber: "",
        pushUrl: "",
        isOwned: true
      }
    });
  };

  render() {
    const { onCancel } = this.props;
    return (
      <form>
        <TextInput
          label="Name"
          name="name"
          value={this.state.device.name}
          onChange={this.updateDevice}
          wraperClasses={["device-name"]}
        />
        <TextInput
          label="Label"
          name="label"
          value={this.state.device.label}
          onChange={this.updateDevice}
          wraperClasses={["device-name"]}
        />
        <div className="add-attr">
          <select
            name="deviceTypeId"
            placeholder="DeviceType"
            value={this.state.device.deviceTypeId}
            onChange={this.updateDevice}
          >
            <option value={""}>Select DeviceType</option>
            {this.props.deviceTypes.map(item => (
              <option key={item.id} value={item.id}>
                {item.name}
              </option>
            ))}
          </select>
        </div>
        <TextInput
          label="SerialNumber"
          name="serialNumber"
          value={this.state.device.serialNumber}
          onChange={this.updateDevice}
          wraperClasses={["device-name"]}
        />
        <TextInput
          label="PushUrl"
          name="pushUrl"
          value={this.state.device.pushUrl}
          onChange={this.updateDevice}
          wraperClasses={["device-name"]}
        />
        <button
          type="button"
          className="normal-btn save-btn"
          onClick={this.addNewDevice}
        >
          Save
        </button>
        <button
          type="button"
          className="no-border-btn cancel-btn"
          onClick={onCancel}
        >
          Cancel
        </button>
      </form>
    );
  }
}

export default NewDeviceForm;
