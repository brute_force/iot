import React, { Component } from "react";
import DeviceItem from "./DeviceItem";
import "../../assets/css/deviceTypeList.css";

class DeviceTypeList extends Component {
  constructor(props) {
    super(props);

    const items = {};
    for (let index = 0; index < this.props.list.length; index++) {
      const item = this.props.list[index];
      items[item.id] = false;
    }
    this.state = { items };
  }

  componentDidUpdate(prevProps) {
    if (this.props.list.length !== prevProps.list.length) {
      this.toggleExpand();
    }
  }

  toggle = item => {
    const preState = this.state.items[item.id];
    this.toggleExpand(false, () => {
      this.setState(state => ({
        items: {
          ...state.items,
          [item.id]: !preState
        }
      }));
      if (!preState) {
        this.props.showDetails(item);
      }
    });
  };

  toggleExpand(expand = false, callback) {
    const items = {};
    for (let index = 0; index < this.props.list.length; index++) {
      const item = this.props.list[index];
      items[item.id] = expand;
    }
    this.setState({ items }, callback && callback);
  }

  deleteItem = item => this.props.onDeleteItem(item.id);

  editItem = item => this.props.onEditItem(item);

  viewSandbox = item => this.props.onSandbox(item);

  viewDataModal = item => this.props.onDataModal(item);

  render() {
    return (
      <dl className="accordion">
        {this.props.list.map((item, i) => {
          return (
            <DeviceItem
              key={item.id}
              row={i + 1}
              item={item}
              expand={this.state.items[item.id]}
              onToggle={this.toggle}
              onDeleteItem={this.deleteItem}
              onEditItem={this.editItem}
              onSandbox={this.viewSandbox}
              onDataModal={this.viewDataModal}
            />
          );
        })}
      </dl>
    );
  }
}

export default DeviceTypeList;
