import React, { Component } from "react";
import Spinner from "../UI/Spinner";
import editIcon from "../../assets/icons/edit.svg";
import dataIcon from "../../assets/icons/data.svg";

class DeviceItem extends Component {
  toggle = () => this.props.onToggle(this.props.item);

  deleteItem = e => {
    e.stopPropagation();
    this.props.onDeleteItem(this.props.item);
  };

  editItem = e => {
    e.stopPropagation();
    this.props.onEditItem(this.props.item);
  };

  viewSandbox = e => {
    e.stopPropagation();
    this.props.onSandbox(this.props.item);
  };

  viewDataModal = e => {
    e.stopPropagation();
    this.props.onDataModal(this.props.item);
  };

  render() {
    const { row, item, expand, onToggle } = this.props;

    return (
      <>
        <dt
          className={expand ? "title is-expanded" : "title"}
          onClick={this.toggle}
        >
          <i className="header row-number">{row}</i>
          <i className="header row-name">{item.name}</i>
          <span className="tools">
            <i
              style={{ display: "none" }}
              className="trash"
              onClick={e => e.stopPropagation()}
            >
              ROLE
            </i>
            <i
              style={{ display: !item.isOwned && "none" }}
              className="trash"
              title="view data"
              onClick={this.viewDataModal}
            >
              <img src={dataIcon} alt="data" />
            </i>
            <i
              style={{ display: !item.isOwned && "none" }}
              className="trash iot-user"
              title="view granted roles"
              onClick={this.viewSandbox}
            />
            <i
              style={{ display: !item.isOwned && "none" }}
              className="trash"
              title="edit device"
              onClick={this.editItem}
            >
              <img src={editIcon} alt="Edit" />
            </i>
            <i
              style={{ display: !item.isOwned && "none" }}
              className="trash iot-trash-bin"
              title="delete device"
              onClick={this.deleteItem}
            />
            <i
              style={{ display: !item.isOwned && "none" }}
              className="toggle iot-circle-arrow-down"
              title="view details"
            />
          </span>
        </dt>
        <dd
          className={
            expand
              ? "accordion-content content is-expanded"
              : "accordion-content content"
          }
          onClick={onToggle}
        >
          <table style={{ width: "100%" }}>
            <tbody>
              <tr>
                <th>ID</th>
                <td>{item.id}</td>
              </tr>
              <tr>
                <th>Name</th>
                <td>{item.name}</td>
              </tr>
              <tr>
                <th>DeviceTypeId</th>
                {item.hasDetail ? (
                  <td>{item.deviceTypeId}</td>
                ) : (
                  <td>
                    <Spinner />
                  </td>
                )}
              </tr>
              <tr>
                <th>DeviceTypeName</th>
                {item.hasDetail ? (
                  <td>{item.deviceTypeName}</td>
                ) : (
                  <td>
                    <Spinner />
                  </td>
                )}
              </tr>
              <tr>
                <th>SerialNumber</th>
                {item.hasDetail ? (
                  <td>{item.serialNumber}</td>
                ) : (
                  <td>
                    <Spinner />
                  </td>
                )}
              </tr>
              <tr>
                <th>EncryptionKey</th>
                {item.hasDetail ? (
                  <td>{item.encryptionKey}</td>
                ) : (
                  <td>
                    <Spinner />
                  </td>
                )}
              </tr>
              <tr>
                <th>DeviceToken</th>
                {item.hasDetail ? (
                  <td>{item.deviceToken}</td>
                ) : (
                  <td>
                    <Spinner />
                  </td>
                )}
              </tr>
            </tbody>
          </table>
        </dd>
      </>
    );
  }
}

export default DeviceItem;
