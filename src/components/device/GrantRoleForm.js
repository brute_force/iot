import React, { Component } from "react";

class GrantRoleForm extends Component {
  state = {
    roleId: "",
    username: ""
  };

  handleChanges = ({ target }) => {
    this.setState({
      [target.name]: target.value
    });
  };

  grantRole = () => {
    this.props.onGrantRole(this.state);
    this.setState({
      roleId: "",
      username: ""
    });
  };

  render() {
    return (
      <div className="add-attr">
        <input
          type="text"
          name="username"
          placeholder="Username"
          value={this.state.username}
          onChange={this.handleChanges}
        />
        <select
          name="roleId"
          placeholder="Role"
          value={this.state.roleId}
          onChange={this.handleChanges}
        >
          <option value={""}>Select Role</option>
          {this.props.roles.map(r => {
            if (r.deviceTypeId) {
              return (
                <option key={r.id} value={r.id}>
                  {r.name}
                </option>
              );
            } else return null;
          })}
        </select>
        <button
          type="button"
          className="border-btn add-attr-btn"
          onClick={this.grantRole}
        >
          + Add
        </button>
      </div>
    );
  }
}

export default GrantRoleForm;
