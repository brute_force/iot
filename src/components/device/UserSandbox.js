import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as actions from "../../actions/grantedRolesActions";
import GrantRoleForm from "./GrantRoleForm";
import Spinner from "../UI/Spinner";

class UserSandbox extends Component {
  componentDidUpdate(prevProps) {
    if (this.props.deviceId !== prevProps.deviceId) {
      this.props.actions.getGrantedRoles(this.props.deviceId);
    }
  }

  shouldComponentUpdate(nextProps) {
    return nextProps.isVisible;
  }

  grantRole = grantedRole => {
    this.props.actions.grantRole({
      ...grantedRole,
      deviceId: this.props.deviceId,
      roleName: this.props.roles.find(r => r.id === grantedRole.roleId).name
    });
  };

  takeRole = role => {
    this.props.actions.takeRole(role);
  };

  render() {
    const { grantedRoles, onCancel } = this.props;
    return (
      <form>
        <div className="elem-warp device-attr">
          <div className="device-attr-table" style={{ width: "100%" }}>
            <table id="granted-roles-table">
              <tbody>
                <tr>
                  <th>User</th>
                  <th>Role</th>
                  <th />
                </tr>
                {this.props.loading ? (
                  <tr>
                    <td colSpan="3">
                      <Spinner />
                    </td>
                  </tr>
                ) : grantedRoles.length ? (
                  grantedRoles.map((role, index) => (
                    <tr key={index}>
                      <td>{role.username}</td>
                      <td>{role.roleName}</td>
                      <td>
                        <i
                          style={{ cursor: "pointer" }}
                          className="trash iot-trash-bin"
                          onClick={() => this.takeRole(role)}
                        />
                      </td>
                    </tr>
                  ))
                ) : (
                  <tr>
                    <td colSpan="3">No Items Available</td>
                  </tr>
                )}
              </tbody>
            </table>
            <GrantRoleForm
              roles={this.props.roles}
              onGrantRole={this.grantRole}
            />
          </div>
        </div>
        <button
          type="button"
          className="normal-btn save-btn"
          onClick={onCancel}
        >
          Close
        </button>
      </form>
    );
  }
}
function mapStateToProps(state, props) {
  return {
    ...props,
    grantedRoles: state.grantedRoles,
    loading: state.ui.spinner
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ ...actions }, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserSandbox);
