import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as actions from "../../actions/deviceDataActions";
import axios from "axios";
import DeviceApi from "../../api/device.api";
import DeviceTypeApi from "../../api/deviceType.api";
import RoleApi from "../../api/role.api";
import Spinner from "../UI/Spinner";
import { keyBy, values } from "lodash";

class DeviceData extends Component {
  deviceType = "";
  deviceRole = "";

  state = { loading: false, write: {}, send: {} };

  componentDidUpdate(prevProps) {
    if (this.props.device.id !== prevProps.device.id) {
      this.setState({ loading: true });
      this.props.actions.getDeviceData(this.props.device.id);
      if (this.props.device.hasDetail) {
        this.getDeviceType(this.props.device.deviceTypeId);
      } else {
        DeviceApi.show(this.props.device.id).then(res => {
          this.getDeviceType(res.data.data.deviceTypeId);
        });
      }
    }
  }

  componentWillUnmount() {
    this.deviceType = "";
    this.deviceRole = "";
  }

  getDeviceType = deviceTypeId => {
    axios
      .all([DeviceTypeApi.show(deviceTypeId), RoleApi.show(deviceTypeId)])
      .then(
        axios.spread((dtRes, roleRes) => {
          this.deviceType = dtRes.data.data;
          this.deviceRole = keyBy(
            roleRes.data.data.attributePermissions,
            "attributeTypeName"
          );
          this.setState({ loading: false, write: {}, send: {} });
        })
      );
  };

  handleChanges = ({ target }) => {
    if ((this.deviceRole[target.name] || {}).permission === "N") {
      const write = this.state.write;
      this.setState({
        write: {
          ...write,
          [target.name]: { name: target.name, value: target.value }
        }
      });
    } else {
      const send = this.state.send;
      this.setState({
        send: {
          ...send,
          [target.name]: { name: target.name, value: target.value }
        }
      });
    }
  };

  send = () => {
    const write = values(this.state.write);
    const send = values(this.state.send);
    const deviceId = this.props.device.id;
    if (write.length) {
      this.props.actions.writeToDevice({ deviceId, write });
    }
    if (send.length) {
      this.props.actions.sendToDevice({ deviceId, send });
    }
    this.setState({ loading: false, write: {}, send: {} });
    this.props.actions.resetDeviceData();
  };

  shouldComponentUpdate(nextProps) {
    return nextProps.isVisible;
  }

  render() {
    const { deviceData, onCancel } = this.props;
    return (
      <form>
        <div className="elem-warp device-attr">
          <div className="device-attr-table" style={{ width: "100%" }}>
            <div className="add-attr">
              <table id="granted-roles-table">
                <tbody>
                  <tr>
                    <th>Name</th>
                    <th>Type</th>
                    <th>Per</th>
                    <th>Value</th>
                  </tr>
                  {this.state.loading ? (
                    <tr>
                      <td colSpan="4">
                        <Spinner />
                      </td>
                    </tr>
                  ) : ((this.deviceType || {}).attributeTypes || []).length ? (
                    this.deviceType.attributeTypes.map((attr, index) => (
                      <tr key={index}>
                        <td>{attr.name}</td>
                        <td>
                          {Array.isArray(attr.type)
                            ? `[${attr.type.join(", ")}]`
                            : attr.type}
                        </td>
                        <td>{(this.deviceRole[attr.name] || {}).permission}</td>
                        <td>
                          <input
                            style={{ width: "50%" }}
                            type="text"
                            name={attr.name}
                            value={(deviceData[attr.name] || {}).value}
                            onChange={this.handleChanges}
                          />
                        </td>
                      </tr>
                    ))
                  ) : (
                    <tr>
                      <td colSpan="4">No Items Available</td>
                    </tr>
                  )}
                </tbody>
              </table>
            </div>
          </div>
        </div>
        {((this.deviceType || {}).attributeTypes || []).length && (
          <button
            type="button"
            className="normal-btn save-btn"
            onClick={this.send}
          >
            SEND
          </button>
        )}
        <button type="button" className="no-border-btn" onClick={onCancel}>
          Close
        </button>
      </form>
    );
  }
}

function mapStateToProps(state, props) {
  return {
    ...props,
    deviceData: keyBy(state.deviceData, "name")
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ ...actions }, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DeviceData);
