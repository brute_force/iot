import React, { Component } from "react";
import TextInput from "../UI/TextInput";

class EditDeviceForm extends Component {
  shouldComponentUpdate(nextProps) {
    return nextProps.isVisible;
  }

  render() {
    const { device, onSave, onCancel } = this.props;
    return (
      <form>
        <TextInput
          label="Name"
          name="name"
          value={device.name}
          onChange={this.props.updateDevice}
          wraperClasses={["device-name"]}
        />
        <TextInput
          label="SerialNumber"
          name="serialNumber"
          value={device.serialNumber}
          onChange={this.props.updateDevice}
          wraperClasses={["device-name"]}
        />
        <button type="button" className="normal-btn save-btn" onClick={onSave}>
          Save
        </button>
        <button
          type="button"
          onClick={onCancel}
          className="no-border-btn cancel-btn"
        >
          Cancel
        </button>
      </form>
    );
  }
}

export default EditDeviceForm;
