import React from "react";
import Spinner from "../UI/Spinner";

const AttributeTypes = ({
  attributePermissions = [],
  onChangePermission,
  loading
}) => (
  <div className="elem-warp device-attr" style={{ marginBottom: "60px" }}>
    <label>Attribute Permisions</label>
    <div className="device-attr-table">
      <table>
        <tbody>
          <tr>
            <th>Name</th>
            <th>Permision</th>
          </tr>
          {loading ? (
            <tr>
              <td colSpan="2">
                <Spinner />
              </td>
            </tr>
          ) : attributePermissions.length ? (
            attributePermissions.map((attrPer, index) => (
              <AttributePermision
                key={attrPer.attributeTypeName + index}
                attrPer={attrPer}
                onChangePermission={onChangePermission}
              />
            ))
          ) : (
            <tr>
              <td colSpan="2">No Items Available</td>
            </tr>
          )}
        </tbody>
      </table>
    </div>
  </div>
);

const AttributePermision = props => {
  const onChangePermission = ({ target }) =>
    props.onChangePermission({
      attributeTypeName: attrPer.attributeTypeName,
      permission: target.value
    });

  const { attrPer } = props;
  return (
    <tr>
      <td>{attrPer.attributeTypeName}</td>
      <td>
        <select
          className="select"
          name="permission"
          placeholder="Permission"
          value={attrPer.permission}
          onChange={onChangePermission}
        >
          <option value="N">N</option>
          <option value="R">R</option>
          <option value="W">W</option>
          <option value="RW">RW</option>
        </select>
      </td>
    </tr>
  );
};

export default AttributeTypes;
