import React, { Component } from "react";
import RoleItem from "./RoleItem";
import "../../assets/css/deviceTypeList.css";

class RoleList extends Component {
  constructor(props) {
    super(props);

    const items = {};
    for (let index = 0; index < this.props.list.length; index++) {
      const item = this.props.list[index];
      items[item.id] = false;
    }
    this.state = { items };
  }

  componentDidUpdate(prevProps) {
    if (this.props.list.length !== prevProps.list.length) {
      this.toggleExpand();
    }
  }

  toggle = item => {
    const preState = this.state.items[item.id];
    this.toggleExpand(false, () => {
      this.setState(state => ({
        items: {
          ...state.items,
          [item.id]: !preState
        }
      }));
      if (!preState) {
        this.props.showDetails(item);
      }
    });
  };

  toggleExpand(expand = false, callback) {
    const items = {};
    for (let index = 0; index < this.props.list.length; index++) {
      const item = this.props.list[index];
      items[item.id] = expand;
    }
    this.setState({ items }, callback && callback);
  }

  render() {
    return (
      <dl className="accordion">
        {this.props.list.map((item, i) => {
          return (
            <RoleItem
              key={item.id}
              row={i + 1}
              item={item}
              expand={this.state.items[item.id]}
              onClick={() => this.toggle(item)}
              onDeleteItem={() => this.props.onDeleteItem(item.id)}
              onEditItem={() => this.props.onEditItem(item)}
            />
          );
        })}
      </dl>
    );
  }
}

export default RoleList;
