import React, { Component } from "react";
import TextInput from "../UI/TextInput";
import TextArea from "../UI/TextArea";
import AttributePermissions from "./AttributePermisions";
import DeviceTypeApi from "../../api/deviceType.api";

class NewRoleForm extends Component {
  state = {
    role: {
      name: "",
      deviceTypeId: "",
      description: "",
      attributePermissions: []
    },
    loading: false,
    deviceType: ""
  };

  handleChanges = ({ target }) => {
    const role = { ...this.state.role, [target.name]: target.value };
    this.setState({
      role
    });
  };

  handleDeviceTypeChanges = ({ target }) => {
    const role = { ...this.state.role, deviceTypeId: target.value };

    if (!role.deviceTypeId) {
      return;
    }

    this.setState({
      role,
      loading: true
    });

    DeviceTypeApi.show(target.value).then(res => {
      const role = {
        ...this.state.role,
        attributePermissions: res.data.data.attributeTypes.map(attr => ({
          attributeTypeName: attr.name,
          permission: "N"
        }))
      };

      this.setState({
        role,
        deviceType: res.data,
        loading: false
      });
    });
  };

  onChangePermission = attrPer => {
    const role = {
      ...this.state.role,
      attributePermissions: this.state.role.attributePermissions.map(item => {
        if (item.attributeTypeName === attrPer.attributeTypeName) {
          return {
            attributeTypeName: item.attributeTypeName,
            permission: attrPer.permission
          };
        }
        return item;
      })
    };
    this.setState({
      role
    });
  };

  addRole = () => {
    this.props.onSave(this.state.role);
    this.setState({
      role: {
        name: "",
        deviceTypeId: "",
        description: "",
        attributePermissions: []
      },
      loading: false,
      deviceType: ""
    });
  };

  render() {
    const { onCancel } = this.props;
    return (
      <form>
        <TextInput
          label="Name"
          name="name"
          value={this.state.role.name}
          onChange={this.handleChanges}
          wraperClasses={["device-name"]}
        />
        <div className="add-attr">
          <select
            name="deviceTypeId"
            placeholder="DeviceType"
            value={this.state.role.deviceTypeId}
            onChange={this.handleDeviceTypeChanges}
          >
            <option value={""}>Select DeviceType</option>
            {this.props.deviceTypes.map(item => (
              <option key={item.id} value={item.id}>
                {item.name}
              </option>
            ))}
          </select>
        </div>
        <TextArea
          label="Desciption"
          name="description"
          value={this.state.role.description}
          onChange={this.handleChanges}
          wraperClasses={["device-name"]}
        />
        <AttributePermissions
          attributePermissions={this.state.role.attributePermissions}
          onChangePermission={this.onChangePermission}
          loading={this.state.loading}
        />
        <button
          type="button"
          className="normal-btn save-btn"
          onClick={this.addRole}
        >
          Save
        </button>
        <button
          type="button"
          onClick={onCancel}
          className="no-border-btn cancel-btn"
        >
          Cancel
        </button>
      </form>
    );
  }
}

export default NewRoleForm;
