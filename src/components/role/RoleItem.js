import React, { Component } from "react";
import Spinner from "../UI/Spinner";
import editIcon from "../../assets/icons/edit.svg";

class RoleItem extends Component {
  render() {
    const { row, item, expand, onClick } = this.props;

    return (
      <>
        <dt
          className={expand ? "title is-expanded" : "title"}
          onClick={onClick}
        >
          <i className="header row-number">{row}</i>
          <i className="header row-name">{item.name}</i>
          <span className="tools">
            <i
              className="trash"
              onClick={e => {
                e.stopPropagation();
                this.props.onEditItem();
              }}
            >
              <img src={editIcon} alt="Edit" />
            </i>
            <i
              className="trash iot-trash-bin"
              onClick={e => {
                e.stopPropagation();
                this.props.onDeleteItem();
              }}
            />
            <i className="toggle iot-circle-arrow-down" />
          </span>
        </dt>
        <dd
          className={
            expand
              ? "accordion-content content is-expanded"
              : "accordion-content content"
          }
          onClick={onClick}
        >
          <table style={{ width: "100%" }}>
            <tbody>
              <tr>
                <th>ID</th>
                <td>{item.id}</td>
              </tr>
              <tr>
                <th>Name</th>
                <td>{item.name}</td>
              </tr>
              <tr>
                <th>DeviceTypeId</th>
                <td>{item.deviceTypeId}</td>
              </tr>
              <tr>
                <th>DeviceTypeName</th>
                <td>{item.deviceTypeName}</td>
              </tr>
              <tr>
                <th>Description</th>
                {item.hasDetail ? (
                  <td>{item.description}</td>
                ) : (
                  <td>
                    <Spinner />
                  </td>
                )}
              </tr>
              <tr>
                <th>Attribute Permissions </th>
                {item.hasDetail ? (
                  <td className="sub-table">
                    <table style={{ width: "100%" }}>
                      <tbody>
                        <tr>
                          <th>Name</th>
                          <th>Permission</th>
                        </tr>
                        {(item.attributePermissions || []).map((attr, i) => (
                          <tr key={i}>
                            <th>{attr.attributeTypeName}</th>
                            <th>{attr.permission}</th>
                          </tr>
                        ))}
                      </tbody>
                    </table>
                  </td>
                ) : (
                  <Spinner />
                )}
              </tr>
            </tbody>
          </table>
        </dd>
      </>
    );
  }
}

export default RoleItem;
