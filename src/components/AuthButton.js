import React, { Component } from "react";
import axios from "axios";
import { withRouter } from "react-router-dom";
import { qsToObject } from "../util";

const sso = {
  grant_type: "authorization_code",
  client_id: process.env.REACT_APP_CLIENT_ID,
  client_secret: process.env.REACT_APP_CLIENT_SECRET,
  code: "",
  redirect_uri: process.env.REACT_APP_REDIRECT_URI || window.location.origin
};

class AuthButton extends Component {
  state = {
    isLoggedIn: !!localStorage.getItem("userToken")
  };

  componentDidMount() {
    const params = qsToObject(this.props.location.search.slice(1));

    if (params && params.code) {
      localStorage.setItem("authCode", params.code);
      sso.code = params.code;
      const startReq = Date.now();
      axios
        .post("https://accounts.pod.land/oauth2/token", null, { params: sso })
        .then(res => res.data)
        .then(({ access_token, refresh_token, expires_in }) => {
          axios
            .get(`https://accounts.pod.land/users`, {
              headers: { Authorization: `Bearer ${access_token}` }
            })
            .then(res => {
              const now = Date.now();
              const diff = now - startReq;
              const expire = now - diff - 100 + expires_in * 1000;
              localStorage.setItem("expires_in", expire);
              localStorage.setItem("userToken", access_token);
              localStorage.setItem("refreshToken", refresh_token);
              this.setState({ isLoggedIn: true });
              localStorage.setItem("username", res.data.preferred_username);
              this.props.history.replace("/");
            });
        });
    }
  }

  signout = e => {
    e.preventDefault();
    localStorage.removeItem("authCode");
    localStorage.removeItem("expires_in");
    localStorage.removeItem("userToken");
    localStorage.removeItem("refreshToken");
    this.setState({ isLoggedIn: false });
    this.props.history.replace("/");
  };

  render() {
    return this.state.isLoggedIn ? (
      <a className="login-btn border-btn" href="*" onClick={this.signout}>
        <i className="iot-user" />
        <span>{localStorage.getItem("username")}</span>
      </a>
    ) : (
      <a
        className="login-btn border-btn"
        href={`https://accounts.pod.land/oauth2/authorize?response_type=code&client_id=${
          sso.client_id
        }&redirect_uri=${sso.redirect_uri}`}
      >
        <i className="iot-user" />
        <span>Login</span>
      </a>
    );
  }
}

export default withRouter(AuthButton);
