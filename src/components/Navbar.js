import React from "react";
import { Link } from "react-router-dom";

const Navbar = ({ activeRoute }) => {
  const isActive = path => (activeRoute === path ? "active" : "");

  return (
    <nav className="navigation">
      <ul>
        <li className={isActive("/deviceTypes")}>
          <Link to="/deviceTypes">Device Type</Link>
        </li>
        <li className={isActive("/devices")}>
          <Link to="/devices">Devices</Link>
        </li>
        <li className={isActive("/roles")}>
          <Link to="/roles">Roles</Link>
        </li>
      </ul>
    </nav>
  );
};

export default Navbar;
