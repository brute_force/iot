import React from "react";

const TextArea = (props) => {
  const {
    name,
    label,
    onChange,
    placeholder,
    value,
    cols = 60,
    rows = 1,
    wraperClasses
  } = props;
  
  return (
  <div className={`elem-wrap ${wraperClasses.join(" ")}`}>
    <label htmlFor={`textArea-${name}`}>{label}</label>
    <textarea
      id={`textArea-${name}`}
      name={name}
      value={value}
      placeholder={placeholder}
      onChange={onChange}
      cols={cols}
      rows={rows}
    />
  </div>
)};

export default TextArea;
