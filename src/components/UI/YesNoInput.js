import React from "react";

const YesNoInput = ({ label, value, onChange, wraperClasses = [] }) => (
  <div className={`elem-wrap ${wraperClasses.join(" ")}`}>
    <label>{label}</label>
    <label>
      <input
        type="radio"
        name="encryptionEnabled"
        value={true}
        checked={!!value}
        onChange={onChange}
      />
      Yes
    </label>
    <label>
      <input
        type="radio"
        name="encryptionEnabled"
        value={false}
        checked={!!!value}
        onChange={onChange}
      />
      No
    </label>
  </div>
);

export default YesNoInput;
