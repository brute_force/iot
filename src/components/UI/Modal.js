import React from "react";

const Modal = ({ isVisible, toggleVisibility, title, children }) => (
  <div className={isVisible ? "overlay active" : "overlay"}>
    <div className="add-new-device">
      <div className="back-to-list" onClick={toggleVisibility}>
        <i className="iot-go-back-left-arrow" />
        <span>back to list</span>
      </div>
      <h2 className="add-new-device-title">{title}</h2>
      {children}
    </div>
  </div>
);

export default Modal;
