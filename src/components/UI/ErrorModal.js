import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as uiActions from "../../actions/uiActions";

class ErrorModal extends Component {
  closeErrorModal = () => {
    this.props.actions.closeErrorModal();
  };

  render() {
    const { hasError, errorMessage } = this.props;

    return (
      <div
        className={hasError ? "active overlay" : "overlay"}
        style={{ display: hasError ? "block" : "none" }}
      >
        <div className="alert-box">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="100"
            height="92"
            viewBox="0 0 100 92"
          >
            <g fill="#FFF">
              <path d="M99.088 78.829l-.001-.002C92.429 65.171 81.27 45.803 71.424 28.715c-5.151-8.944-10.018-17.389-13.536-23.808-1.66-3.031-4.61-4.842-7.89-4.842-3.28 0-6.23 1.81-7.89 4.842-3.52 6.423-8.389 14.877-13.546 23.826C18.722 45.817 7.567 65.177.912 78.83A8.88 8.88 0 0 0 0 82.737c0 4.96 4.006 8.995 8.932 8.995L50 91.728l41.068.004c4.926 0 8.932-4.035 8.932-8.995a8.88 8.88 0 0 0-.912-3.908zm-8.02 8.738H8.937c-2.627 0-4.765-2.169-4.765-4.83 0-.712.165-1.414.49-2.083 6.592-13.521 17.708-32.817 27.515-49.84 5.167-8.97 10.048-17.443 13.588-23.905 1.835-3.353 6.637-3.353 8.472 0 3.538 6.458 8.414 14.923 13.578 23.885 9.813 17.03 20.933 36.333 27.527 49.858.328.671.492 1.373.492 2.085 0 2.661-2.138 4.83-4.764 4.83z" />
              <path d="M50 20.9a2.082 2.082 0 0 0-2.083 2.084V64.65a2.082 2.082 0 1 0 4.167 0V22.984A2.082 2.082 0 0 0 50 20.9z" />
              <circle cx="50" cy="77.148" r="2.083" />
            </g>
          </svg>
          <p>{errorMessage}</p>
          <button
            type="button"
            className="border-btn ok-btn"
            style={{ backgroundColor: "#022f80" }}
            onClick={this.closeErrorModal}
          >
            ok
          </button>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state, props) {
  return {
    hasError: state.ui.hasError,
    errorMessage: state.ui.errorMessage
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ ...uiActions }, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ErrorModal);
