import React, { Component } from "react";
import { connect } from "react-redux";
import "../../assets/css/loading.css";

class Loading extends Component {
  render() {
    const { loading } = this.props;

    return (
      <div
        className={loading > 0 ? "active overlay" : "overlay"}
        style={{ display: loading ? "block" : "none" }}
      >
        <div className="alert-box">
          <div className="sk-cube-grid">
            <div className="sk-cube sk-cube1" />
            <div className="sk-cube sk-cube2" />
            <div className="sk-cube sk-cube3" />
            <div className="sk-cube sk-cube4" />
            <div className="sk-cube sk-cube5" />
            <div className="sk-cube sk-cube6" />
            <div className="sk-cube sk-cube7" />
            <div className="sk-cube sk-cube8" />
            <div className="sk-cube sk-cube9" />
          </div>
          <p>Please wait...</p>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    loading: state.ui.loading
  };
}

export default connect(
  mapStateToProps,
  null
)(Loading);
