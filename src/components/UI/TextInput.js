import React from "react";

const TextInput = props => {
  const {
    label,
    name,
    value,
    placeholder,
    onChange,
    wraperClasses = []
  } = props;

  return (
    <div className={`elem-wrap ${wraperClasses.join(" ")}`}>
      <label htmlFor={`textInput-${name}`}>{label}</label>
      <input
        type="text"
        id={`textInput-${name}`}
        name={name}
        value={value}
        placeholder={placeholder}
        onChange={onChange}
      />
    </div>
  );
};

export default TextInput;
