import React from "react";
import "../../assets/css/spinner.css";

const Spinner = props => {
  return (
    <div className="loader">
      <span className="loader-item" />
      <span className="loader-item" />
      <span className="loader-item" />
      <span className="loader-item" />
      <span className="loader-item" />
      <span className="loader-item" />
      <span className="loader-item" />
      <span className="loader-item" />
      <span className="loader-item" />
      <span className="loader-item" />
    </div>
  );
};

export default Spinner;
