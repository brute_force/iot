import React, { Component } from "react";
import Spinner from "../UI/Spinner";

class DeviceTypeItem extends Component {
  render() {
    const { row, item, expand, onClick } = this.props;

    return (
      <>
        <dt
          className={expand ? "title is-expanded" : "title"}
          onClick={onClick}
        >
          <i className="header row-number">{row}</i>
          <i className="header row-name">{item.name}</i>
          <span className="tools">
            <i
              className="trash iot-trash-bin"
              onClick={e => {
                e.stopPropagation();
                this.props.onDeleteItem();
              }}
            />
            <i className="toggle iot-circle-arrow-down" />
          </span>
        </dt>
        <dd
          className={
            expand
              ? "accordion-content content is-expanded"
              : "accordion-content content"
          }
          onClick={onClick}
        >
          <table style={{ width: "100%" }}>
            <tbody>
              <tr>
                <th>ID</th>
                <td>{item.id}</td>
              </tr>
              <tr>
                <th>Name</th>
                <td>{item.name}</td>
              </tr>
              <tr>
                <th>Encrypted</th>
                {item.hasDetail ? (
                  <td>{item.encrypted ? "Yes" : "No"}</td>
                ) : (
                  <td>
                    <Spinner />
                  </td>
                )}
              </tr>
              <tr>
                <th>Description</th>
                {item.hasDetail ? (
                  <td>
                    <p>{item.description}</p>
                  </td>
                ) : (
                  <Spinner />
                )}
              </tr>
              <tr>
                <th>Attribute Types</th>
                {item.hasDetail ? (
                  <td className="sub-table">
                    <table style={{ width: "100%" }}>
                      <tbody>
                        <tr>
                          <th>Name</th>
                          <th>Type</th>
                        </tr>
                        {(item.attributeTypes || []).map(attr => (
                          <tr>
                            <th>{attr.name}</th>
                            <th>
                              {Array.isArray(attr.type)
                                ? `[${attr.type.join(", ")}]`
                                : attr.type}
                            </th>
                          </tr>
                        ))}
                      </tbody>
                    </table>
                  </td>
                ) : (
                  <Spinner />
                )}
              </tr>
            </tbody>
          </table>
        </dd>
      </>
    );
  }
}

export default DeviceTypeItem;
