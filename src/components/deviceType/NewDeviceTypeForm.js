import React from "react";
import TextInput from "../UI/TextInput";
import TextArea from "../UI/TextArea";
import YesNoInput from "../UI/YesNoInput";
import AttributeTypes from "./AttributeTypes";

const NewDeviceTypeForm = ({ device, onChange, onSave, onCancel }) => {
  return (
    <form>
      <TextInput
        label="Name"
        name="name"
        value={device.name}
        onChange={onChange}
        wraperClasses={["device-name"]}
      />
      <TextArea
        label="Description"
        name="description"
        value={device.description}
        onChange={onChange}
        wraperClasses={["device-desc"]}
      />
      <YesNoInput
        label="Encrypted"
        value={device.encryptionEnabled}
        onChange={onChange}
        wraperClasses={["device-encrypt"]}
      />
      <AttributeTypes attributeTypes={device.attributeTypes} />
      <button type="button" className="normal-btn save-btn" onClick={onSave}>
        Save
      </button>
      <button
        type="button"
        onClick={onCancel}
        className="no-border-btn cancel-btn"
      >
        Cancel
      </button>
    </form>
  );
};

export default NewDeviceTypeForm;
