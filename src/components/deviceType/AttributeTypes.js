import React from "react";
import NewAttributeType from "../../containers/deviceType/NewAttributeType";

const AttributeTypes = ({ attributeTypes = [] }) => (
  <div className="elem-warp device-attr">
    <label>Attribute Types</label>
    <div className="device-attr-table">
      <table>
        <tbody>
          <tr>
            <th>Name</th>
            <th>Type</th>
          </tr>
          {attributeTypes.length ? (
            attributeTypes.map((attrType, index) => (
              <AttributeType key={attrType.name + index} attrType={attrType} />
            ))
          ) : (
            <tr>
              <td colSpan="2">No Items Available</td>
            </tr>
          )}
        </tbody>
      </table>
      <NewAttributeType />
    </div>
  </div>
);

const AttributeType = ({ attrType }) => (
  <tr>
    <td>{attrType.name}</td>
    <td>
      {Array.isArray(attrType.type)
        ? `[${attrType.type.join(", ")}]`
        : attrType.type}
    </td>
  </tr>
);

export default AttributeTypes;
