import React from "react";
import { Link } from "react-router-dom";
import AuthButton from "./AuthButton";
import fanapIcon from "../assets/icons/fanap.svg";

const Header = () => (
  <header className="header">
    <Link className="logo" to="/">
      <img src={fanapIcon} alt="fanap icon" />
      <span>Fanap IOT</span>
    </Link>
    <AuthButton />
  </header>
);

export default Header;
