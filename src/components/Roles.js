import React from "react";
import Navbar from "./Navbar";

const Roles = ({ match }) => (
  <div>
    <Navbar activeRoute={match.path} />
  </div>
);

export default Roles;
