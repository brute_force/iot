import React from "react";

const Titlebar = ({ count, name, toggleNewModal }) => (
  <div className="sub-header">
    <div className="sub-header-left">
      <span className="device-count">{count}</span>
      <p className="device-desc">
        {count > 1 ? `${name}s` : name} {count > 1 ? "are" : "is"} available
      </p>
    </div>
    <div className="sub-header-right">
      <div className="search-box">
        <form>
          <i className="iot-magnifying-glass" />
          <input className="search-input" type="text" placeholder="SEARCH" />
        </form>
      </div>
      <button
        type="button"
        className="normal-btn new-device-btn"
        onClick={toggleNewModal}
      >
        <i className="iot-hex-plus" />
        <span>New {name}</span>
      </button>
    </div>
  </div>
);

export default Titlebar;
