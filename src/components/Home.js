import React from "react";
import { Link } from "react-router-dom";

const Home = () => (
  <nav className="main-menu">
    <ul>
      <li>
        <Link to="/deviceTypes">
          <i className="iot-device-types" />
          Device Type
        </Link>
      </li>
      <li>
        <Link to="/devices">
          <i className="iot-cubes" />
          Devices
        </Link>
      </li>
      <li>
        <Link to="/roles">
          <i className="iot-network" />
          <span>Roles</span>
        </Link>
      </li>
    </ul>
  </nav>
);

export default Home;
