import React, { Component } from "react";
import { Route, Switch } from "react-router-dom";
import Header from "./Header";
import Loading from "./UI/Loading";
import ErrorModal from "./UI/ErrorModal";
import Home from "./Home";
import DeviceTypes from "../containers/deviceType/DeviceTypes";
import Devices from "../containers/device/Devices";
import Roles from "../containers/role/Roles";
import PrivateRoute from "../hoc/PrivateRoute";

class App extends Component {
  render() {
    return (
      <div>
        <Header />
        <Loading />
        <ErrorModal />
        <Switch>
          <Route path="/" exact component={Home} />
          <PrivateRoute path="/deviceTypes" component={DeviceTypes} />
          <PrivateRoute path="/devices" component={Devices} />
          <PrivateRoute path="/roles" exact component={Roles} />
        </Switch>
      </div>
    );
  }
}

export default App;
