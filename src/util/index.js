import axios from "axios";

export function refreshToken() {
  if (Date.now() >= localStorage.getItem("expires_in")) {
    const sso = {
      grant_type: "refresh_token",
      client_id: process.env.REACT_APP_CLIENT_ID,
      client_secret: process.env.REACT_APP_CLIENT_SECRET,
      code: localStorage.getItem("authCode"),
      refresh_token: localStorage.getItem("refreshToken")
    };

    const startReq = Date.now();
    return axios
      .post("https://accounts.pod.land/oauth2/token", null, { params: sso })
      .then(res => res.data)
      .then(data => {
        const { access_token, refresh_token, expires_in } = data;
        const now = Date.now();
        const diff = now - startReq;
        const expire = now - diff - 100 + expires_in * 1000;
        localStorage.setItem("expires_in", expire);
        localStorage.setItem("userToken", access_token);
        localStorage.setItem("refreshToken", refresh_token);
        return data;
      });
  }
  return Promise.resolve(localStorage.getItem("userToken"));
}

export function qsToObject(queryParams = "") {
  return queryParams
    .split("&")
    .map(str => {
      let [key, value] = str.split("=");
      return { [key]: decodeURI(value) };
    })
    .reduce((prev, curr) => Object.assign(prev, curr));
}

export function objectToFormData(obj = {}) {
  const formData = new FormData();
  for (const key in obj) {
    if (obj.hasOwnProperty(key)) {
      let val = obj[key];
      val = typeof val === "object" ? JSON.stringify(val) : val;
      formData.append(key, val);
    }
  }
  return formData;
}

export function objectToQS(obj = {}) {
  let qs = [];
  for (const key in obj) {
    if (obj.hasOwnProperty(key)) {
      let val = obj[key];
      val = typeof val === "object" ? JSON.stringify(val) : val;
      qs.push(`${key}=${val}`);
    }
  }
  return qs.join("&");
}
