# HOW TO DEPLOY IOT

## instal Node.js

- download and install NodeJS from [here](https://nodejs.org/dist/v10.14.1/node-v10.14.1-x64.msi)

## clone project

- clone the project's source from [here](https://gitlab.com/brute_force/iot)
- run `npm install` in the root directory of project

## set subdomain

- if app is not supposed to be hosted at the server root, you should set `homepage` in `package.json` file

  ```
   "homepage": "http://website.com/relativepath"
  ```

## set global variables

- set corresponding (client_id and client_secret) global variables in `.env.production` file

## build project

- run `npm build` in the root directory of project, this will generate `build` folder in project
- copy content of `build` folder into your server
